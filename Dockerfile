FROM node:10-alpine as builder
ENV BUILD_FOLDER=/usr/src/app
WORKDIR ${BUILD_FOLDER}
COPY package*.json ${BUILD_FOLDER}
RUN npm install
COPY . ${BUILD_FOLDER}
RUN echo '{ "allow_root": true }' > /root/.bowerrc
RUN npm run build
RUN npm test
RUN ls -l /usr/src/app

FROM nginx:1.15.5-alpine
COPY --from=builder /usr/src/app/public /usr/share/nginx/html